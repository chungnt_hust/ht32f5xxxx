/*
 * gpioUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 * All GPIO pins can be selected as EXTI trigger source
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "gpioUser.h"


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */

void (*irqHldFunction)(u16 Data);

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
uint8_t gBtnState = 0;


/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
  * @brief  Configure the GPIO ports for input.
  * @retval None
  ***********************************************************************************************************/
void Gpio_In_Configuration(void)
{
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
  GPIO_InitTypeDef gpioInitStruct;
  _GPIO_USER_GPIOA_CLK_ENABLE(CKCUClock);
  _GPIO_USER_GLOBAL_CLK_ENABLE(CKCUClock);

  gpioInitStruct.gpioPort = BTN1_PORTNUM;
  gpioInitStruct.afioPin = (u32)BTN1_PIN;
  gpioInitStruct.afioMode = AFIO_FUN_GPIO;
  gpioInitStruct.htGPIO = BTN1_PORT;
  gpioInitStruct.gpioPin = BTN1_PIN;
  gpioInitStruct.gpioDir = GPIO_DIR_IN;
  gpioInitStruct.gpioPull = GPIO_PR_UP;
  gpioInitStruct.ctrl = ENABLE;

  AFIO_GPxConfig(gpioInitStruct.gpioPort, gpioInitStruct.afioPin, gpioInitStruct.afioMode);
  GPIO_DirectionConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioDir);
  GPIO_PullResistorConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioPull);
  GPIO_InputConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.ctrl);
}

/*********************************************************************************************************//**
  * @brief  Configure the GPIO ports for output.
  * @retval None
  ***********************************************************************************************************/
void Gpio_Out_Configuration(void)
{
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
  GPIO_InitTypeDef gpioInitStruct;
  _GPIO_USER_GPIOA_CLK_ENABLE(CKCUClock);
  _GPIO_USER_GLOBAL_CLK_ENABLE(CKCUClock);
	
  gpioInitStruct.gpioPort = LED1_PORTNUM;
  gpioInitStruct.afioPin = (u32)LED1_PIN;
  gpioInitStruct.afioMode = AFIO_FUN_GPIO;
  gpioInitStruct.htGPIO = LED1_PORT;
  gpioInitStruct.gpioPin = LED1_PIN;
  gpioInitStruct.gpioDir = GPIO_DIR_OUT;
  // gpioInitStruct.gpioPull = GPIO_PR_DISABLE;
  // gpioInitStruct.ctrl = ENABLE;

  AFIO_GPxConfig(gpioInitStruct.gpioPort, gpioInitStruct.afioPin, gpioInitStruct.afioMode);
  GPIO_DirectionConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioDir);
	Gpio_WriteHig_Pin(LED1_PORT, LED1_PIN);
}

FlagStatus Gpio_Read_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n)
{
  return(GPIO_ReadInBit(HT_GPIOx, GPIO_PIN_n));
}

u16 Gpio_Read_Port(HT_GPIO_TypeDef* HT_GPIOx)
{
  return (GPIO_ReadInData(HT_GPIOx));
}

void Gpio_Toggle_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n)
{
  static FlagStatus stt = RESET;
  GPIO_WriteOutBits(HT_GPIOx, GPIO_PIN_n, stt);
  stt = (FlagStatus)!stt;
}

void Gpio_WriteVal_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n, FlagStatus stt)
{
  GPIO_WriteOutBits(HT_GPIOx, GPIO_PIN_n, stt);
}

void Gpio_WriteHig_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n)
{
  GPIO_WriteOutBits(HT_GPIOx, GPIO_PIN_n, SET);
}

void Gpio_WriteLow_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n)
{
  GPIO_WriteOutBits(HT_GPIOx, GPIO_PIN_n, RESET);
}

void Gpio_WriteVal_Port(HT_GPIO_TypeDef* HT_GPIOx, u16 Data)
{
  GPIO_WriteOutData(HT_GPIOx, Data);
}

void Gpio_Interrupt_Configuration(void)
{
  /* Enable peripheral clock */
  CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
  GPIO_InitTypeDef gpioInitStruct;
  _GPIO_USER_GPIOA_CLK_ENABLE(CKCUClock);
  _GPIO_USER_INTERRUPT_ENABLE(CKCUClock);
  _GPIO_USER_GLOBAL_CLK_ENABLE(CKCUClock);

  gpioInitStruct.gpioPort = BTN1_PORTNUM;
  gpioInitStruct.afioPin = (u32)BTN1_PIN;
  gpioInitStruct.afioMode = AFIO_FUN_GPIO;
  gpioInitStruct.htGPIO = BTN1_PORT;
  gpioInitStruct.gpioPin = BTN1_PIN;
  gpioInitStruct.gpioDir = GPIO_DIR_IN;
  gpioInitStruct.gpioPull = GPIO_PR_UP;
  gpioInitStruct.ctrl = ENABLE;

  AFIO_GPxConfig(gpioInitStruct.gpioPort, gpioInitStruct.afioPin, gpioInitStruct.afioMode);
  GPIO_PullResistorConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioPull);
	GPIO_InputConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.ctrl);
	
	/* Select Port as EXTI Trigger Source */
  AFIO_EXTISourceConfig(BTN1_AFIO_EXTI_CH, BTN1_EXTIO_SOURCE);
	
	
	{ /* Configure EXTI Channel n as rising edge trigger */
		/* !!! NOTICE !!!
		 * Notice that the local variable (structure) did not have an initial value.
		 * Please confirm that there are no missing members in the parameter settings below in this function.
		*/
		EXTI_InitTypeDef EXTI_InitStruct;
		EXTI_InitStruct.EXTI_Channel = BTN1_EXTI_CHANNEL;
		EXTI_InitStruct.EXTI_Debounce = EXTI_DEBOUNCE_DISABLE;
		EXTI_InitStruct.EXTI_DebounceCnt = 0;
		EXTI_InitStruct.EXTI_IntType = EXTI_POSITIVE_EDGE;
		EXTI_Init(&EXTI_InitStruct);
	}
	/* Enable EXTI & NVIC line Interrupt */
  EXTI_IntConfig(BTN1_EXTI_CHANNEL, ENABLE);
  NVIC_EnableIRQ(BTN1_EXTI_IRQn);
}

/*********************************************************************************************************//**
 * @brief   This function handles User EXTI interrupt function.
 * @retval  None
 ************************************************************************************************************/
__STATIC_INLINE void BTN1_Process(void)
{

  if(EXTI_GetEdgeFlag(BTN1_EXTI_CHANNEL))
  {
    EXTI_ClearEdgeFlag(BTN1_EXTI_CHANNEL);
		gBtnState = 1;
  }
}

/*********************************************************************************************************//**
 * @brief   This function handles EXTI interrupt.
 * @retval  None
 ************************************************************************************************************/
void BTN1_IRQHandler(void)
{
	BTN1_Process();
}
