/*
 * w25q16jv.c
 *
 *  Created on: Sep 5, 2019
 *      Author: chungnguyen
 */
/**** include: w25q16jv.h ****/
#include "w25q16jv.h"
#include "spiUser.h"
#include "gpioUser.h"
#include "systemClockCfg.h"

static uint8_t writeEN     = 0x06; // Lenh cho phep ghi
static uint8_t writeDIS    = 0x04; // Lenh ko cho phep viet
static uint8_t readDAT     = 0x03; // Lenh doc du lieu
static uint8_t writePage   = 0x02; // Cho phep viet tu 1 byte den 256 byte
static uint8_t sector4k    = 0x20; // Lenh xoa bo nho trong 1 sector 4k-byte
static uint8_t block32k    = 0x52; // 
static uint8_t block64k    = 0xd8;
static uint8_t chipEra     = 0xd8; // Lenh xoa 1 khoi duoc chi dinh 64k-byte
static uint8_t rSR1        = 0x05; // Lenh doc gia tri thanh ghi trang thai SR1
static uint8_t wSR1        = 0x01; // Lenh cho phep ghi thanh ghi trang thai SR1
static uint8_t rSR2        = 0x35;
static uint8_t wSR2        = 0x31;
static uint8_t rSR3        = 0x15;
static uint8_t wSR3        = 0x11;

  
#define pinCS_SET     Gpio_WriteHig_Pin(SPI_SEL1_PORT, SPI_SEL1_PIN) // Lenh Set chan CS = 1;
#define pinCS_RESET   Gpio_WriteLow_Pin(SPI_SEL1_PORT, SPI_SEL1_PIN)  // Lenh Set chan CS = 0;

// Ham gui lenh cho phep ghi 
static void W25Q16JV_writeEnable()
{
	pinCS_RESET;
	SPI_User_SendMulti(&writeEN, 1);
	pinCS_SET;
}

// Ham gui lenh ko cho phep ghi
static void W25Q16JV_writeDisable()
{
	pinCS_RESET;
	SPI_User_SendMulti(&writeDIS, 1);
	pinCS_SET;
}

// Ham doc du lieu
void W25Q16JV_readData(uint32_t add, void *rx_DATA, uint32_t len)
{
	uint8_t *p;
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	pinCS_RESET;
	SPI_User_SendMulti(&readDAT, 1);
	SPI_User_SendMulti(arr, 3);
	
	p = (uint8_t*)rx_DATA;
	SPI_User_ReceiveMulti(p, len);
	
	pinCS_SET;
	//HAL_Delay(100);
	SysClockDelay_ms(100);
}

/* Neu ghi nhieu byte can chu y den vi tri cua byte dang ghi o vi tri nao cua 1 page
 * neu > 255 thi no se quay lai dau page va ghi de du lieu len => Nen ghi tu dia chi 
 * co byte cuoi la 00h
 * Neu ghi 1 byte thi thoai mai :D */ 
static void W25Q16JV_writePage(uint32_t add, uint8_t *tx_DATA, uint32_t len)
{
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	W25Q16JV_writeEnable();
	pinCS_RESET;
	SPI_User_SendMulti(&writePage, 1);
	SPI_User_SendMulti(arr, 3);
	
	SPI_User_SendMulti(tx_DATA, len);
	
	pinCS_SET;
	W25Q16JV_writeDisable();
	// HAL_Delay(100);
	SysClockDelay_ms(100);
}

void W25Q16JV_writeData(uint32_t add, void *tx_DATA, uint32_t len)
{
	uint32_t offsetAdd;
	uint32_t luongDuLieuPhaiGhi;
	uint32_t luongDuLieuDaGhi = 0;
	uint8_t *p;
	
	while(len)
	{
		offsetAdd = add%256;
		if(offsetAdd + len > 256) luongDuLieuPhaiGhi = 256 - offsetAdd;
		else luongDuLieuPhaiGhi = len;
			
		p = (uint8_t*)tx_DATA;
		W25Q16JV_writePage(add, (uint8_t*)&p[luongDuLieuDaGhi], luongDuLieuPhaiGhi);
		add += luongDuLieuPhaiGhi;
		luongDuLieuDaGhi += luongDuLieuPhaiGhi;
		len -= luongDuLieuPhaiGhi;		
	}
}

void W25Q16JV_erase4k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	W25Q16JV_writeEnable();
	pinCS_RESET;
	SPI_User_SendMulti(&sector4k, 1);
	SPI_User_SendMulti(arr, 3);
	pinCS_SET;
	W25Q16JV_writeDisable();
	// HAL_Delay(100);
	SysClockDelay_ms(100);
}

void W25Q16JV_erase32k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	W25Q16JV_writeEnable();
	pinCS_RESET;
	SPI_User_SendMulti(&block32k, 1);
	SPI_User_SendMulti(arr, 3);
	pinCS_SET;
	W25Q16JV_writeDisable();
	// HAL_Delay(500);
	SysClockDelay_ms(500);
}

void W25Q16JV_erase64k(uint32_t add)
{
	uint8_t arr[3];
	arr[0] = add>>16;
	arr[1] = add>>8;
	arr[2] = add;
	
	W25Q16JV_writeEnable();
	pinCS_RESET;
	SPI_User_SendMulti(&block64k, 1);
	SPI_User_SendMulti(arr, 3);
	pinCS_SET;
	W25Q16JV_writeDisable();
	// HAL_Delay(1000);
	SysClockDelay_ms(1000);
}

void W25Q16JV_eraseChip(void)
{
	W25Q16JV_writeEnable();
	pinCS_RESET;
	SPI_User_SendMulti(&chipEra, 1);
	pinCS_SET;
	W25Q16JV_writeDisable();
	// HAL_Delay(2000);
	SysClockDelay_ms(2000);
}

uint8_t W25Q16JV_readSR1(uint8_t bitIndex)
{
	uint8_t value = 0;
	pinCS_RESET;
	SPI_User_SendReceive(rSR1);
	value = SPI_User_SendReceive(0);
	value = (value>>bitIndex)&0x01;
	return value;
}

uint8_t W25Q16JV_readSR2(uint8_t bitIndex)
{
	uint8_t value = 0;
	pinCS_RESET;
	SPI_User_SendReceive(rSR2);
	value = SPI_User_SendReceive(0);
	value = (value>>bitIndex)&0x01;
	return value;
}

uint8_t W25Q16JV_readSR3(uint8_t bitIndex)
{
	uint8_t value = 0;
	pinCS_RESET;
	SPI_User_SendReceive(rSR3);
	value = SPI_User_SendReceive(0);
	value = (value>>bitIndex)&0x01;
	return value;
}

void W25Q16JV_writeSR1(uint8_t value)
{
	W25Q16JV_writeEnable();
	pinCS_RESET;
	SPI_User_SendReceive(wSR1);
	SPI_User_SendReceive(value);
	pinCS_SET;
	W25Q16JV_writeDisable();
}

void W25Q16JV_writeSR2(uint8_t value)
{
	W25Q16JV_writeEnable();
	pinCS_RESET;
	SPI_User_SendReceive(wSR2);
	SPI_User_SendReceive(value);
	pinCS_SET;
	W25Q16JV_writeDisable();
}

void W25Q16JV_writeSR3(uint8_t value)
{
	W25Q16JV_writeEnable();
	pinCS_RESET;
	SPI_User_SendReceive(wSR3);
	SPI_User_SendReceive(value);
	pinCS_SET;
	W25Q16JV_writeDisable();
}

typedef struct
{
	uint8_t x1;
	uint16_t x2;
} dataType_t;
dataType_t tx, rx;

void init(void)
{
	tx.x1 = 123;
	tx.x2 = 11;
}

void W25Q16JV_selftest(void)
{
	init();
	#if 0
	W25Q16JV_erase32k(0x00);
	W25Q16JV_writeData(0x00, &tx, sizeof(tx));
	#else
	W25Q16JV_readData(0x00, &rx, sizeof(rx));
	#endif
}
/****************/













