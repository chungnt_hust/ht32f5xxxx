/*
 * uartUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "uartUser.h"


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
buffer_t bufferRx;

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static void RX_ProcessNewData(void);


/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void UxART_Configuration(void)
{
  { /* Enable peripheral clock of AFIO, UxART                                                               */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
    CKCUClock.Bit.AFIO                   = 1;
    CKCUClock.Bit.RX_GPIO_CLK            = 1;
    CKCUClock.Bit.UART_IPN               = 1;
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Turn on UxART Rx internal pull up resistor to prevent unknow state                                     */
  GPIO_PullResistorConfig(RX_GPIO_PORT, RX_GPIO_PIN, GPIO_PR_UP);

  /* Config AFIO mode as UxART function.                                                                    */
  AFIO_GPxConfig(TX_GPIO_ID, TX_GPIO_PIN, AFIO_FUN_USART_UART); // Tx
  AFIO_GPxConfig(RX_GPIO_ID, RX_GPIO_PIN, AFIO_FUN_USART_UART); // Rx

  {
    /* UxART configured as follow:
          - BaudRate = 115200 baud
          - Word Length = 8 Bits
          - One Stop Bit
          - None parity bit
    */
    USART_InitTypeDef USART_InitStructure = {0};
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WORDLENGTH_8B;
    USART_InitStructure.USART_StopBits = USART_STOPBITS_1;
    USART_InitStructure.USART_Parity = USART_PARITY_NO;
    USART_InitStructure.USART_Mode = USART_MODE_NORMAL;
    USART_Init(UART_PORT, &USART_InitStructure);
  }

  /* Enable UxART interrupt of NVIC                                                                         */
  NVIC_EnableIRQ(UART_IRQ);

  /* Enable UxART Rx interrupt                                                                              */
  USART_IntConfig(UART_PORT, USART_INT_RXDR, ENABLE);

  /* Enable UxART Tx and Rx function                                                                        */
  USART_TxCmd(UART_PORT, ENABLE);
  USART_RxCmd(UART_PORT, ENABLE);
}

void UxART_SendByte(uint8_t xByte)
{
    USART_SendData(UART_PORT, xByte);
		while(USART_GetFlagStatus(UART_PORT, USART_FLAG_TXC) == RESET);
}

void UxART_SendString(uint8_t *str)
{
    while(*str != 0)
    {
        UxART_SendByte(*str);
        str++; 
    }
}

/*************************************************************************************************************
  * @brief  For UxART RX
  * @retval None
  ***********************************************************************************************************/
void UxART_Process(void)
{
	if(bufferRx.State > 0)
	{
		bufferRx.State--;
		if(bufferRx.State == 0)
		{
			RX_ProcessNewData();
		}
	}
}

void UART_IRQ_Hdl(void)
{
    if (USART_GetFlagStatus(UART_PORT, USART_FLAG_RXDR))
    {
        bufferRx.Data[bufferRx.Index] = USART_ReceiveData(UART_PORT);
        bufferRx.Index++;
        bufferRx.State = 3;
    }
}

static void RX_ProcessNewData(void)
{
	bufferRx.Data[bufferRx.Index] = 0;
    DEBUG("%s\r\n", bufferRx.Data);
	// printf("%s\r\n", bufferRx.Data);
	if(bufferRx.Data[0] == '[' && bufferRx.Data[bufferRx.Index-1] == ']')
	{
		
	}
	// else printf("command not support\r\n");

	bufferRx.Index = 0;
}
