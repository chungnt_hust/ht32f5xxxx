/*
 * systemClockCfg.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef SYSTEMCLOCKCFG_H
#define SYSTEMCLOCKCFG_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
typedef void (*sysTickCB)(void);


/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
extern sysTickCB funcSysTickCB;
extern volatile u32 timeDelay;



/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*********************************************************************************************************//**
  * @brief Configure system clock as HSI.
  * @retval None
  ***********************************************************************************************************/
void SysClockCfg_HSI(void);

/*********************************************************************************************************//**
  * @brief Configure system clock as HSE.
  * @retval None
  ***********************************************************************************************************/
void SysClockCfg_HSE(void);

/*********************************************************************************************************//**
  * @brief  Configure the debug output clock.
  * @retval None
  ***********************************************************************************************************/
void SysClockCfg_CKOUTConfig(void);

/*********************************************************************************************************//**
  * @brief  Register New callback function for System tick interrupt.
  * @retval None
  * Call funcSysTickCB() in SysTick_Handler
  void SysTick_Handler(void)
  {
    funcSysTickCB();
  }
  ***********************************************************************************************************/
void SysClockCfg_RegCallback(sysTickCB func);

void SysClockDelay_ms(u32 time);

/*********************************************************************************************************//**
  * @brief  Configure the watchdog timer.
  * @input  mSec must between 0 and 0x0FFF (4095)
  * @retval None
  ***********************************************************************************************************/
void WDT_Configuration(u16 mSec);
#endif
#ifdef __cplusplus
}
#endif
