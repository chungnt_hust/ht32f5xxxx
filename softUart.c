/*
 * softUart.c
 *
 *  Created on: Oct 8, 2019
 *      Author: chungnguyen
 */

#define SOFT_UART_PORTNUM  GPIO_PB
#define SOFT_UART_PORT     HT_GPIOB
#define SOFT_UART_PIN      GPIO_PIN_7

#define Baudrate       4800 //bps
#define OneBitDelay    (1000000/Baudrate)
#define Freq_Mhz       SystemCoreClock
#define fac_us         (Freq_Mhz / 8)
#define UART_TX_PIN(x) Gpio_WriteVal_Pin(SOFT_UART_PORT, SOFT_UART_PIN, x?SET:RESET)

#include "softUart.h"
#include <stdarg.h>
#include <stdlib.h>
#include "gpioUser.h"

void SOFTUART_Configuration(void)
{
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
  GPIO_InitTypeDef gpioInitStruct;
  _GPIO_USER_GPIOB_CLK_ENABLE(CKCUClock);
  _GPIO_USER_GLOBAL_CLK_ENABLE(CKCUClock);
	
  gpioInitStruct.gpioPort = SOFT_UART_PORTNUM;
  gpioInitStruct.afioPin = (u32)SOFT_UART_PIN;
  gpioInitStruct.afioMode = AFIO_FUN_GPIO;
  gpioInitStruct.htGPIO = SOFT_UART_PORT;
  gpioInitStruct.gpioPin = SOFT_UART_PIN;
  gpioInitStruct.gpioDir = GPIO_DIR_OUT;
  // gpioInitStruct.gpioPull = GPIO_PR_DISABLE;
  // gpioInitStruct.ctrl = ENABLE;

  AFIO_GPxConfig(gpioInitStruct.gpioPort, gpioInitStruct.afioPin, gpioInitStruct.afioMode);
  GPIO_DirectionConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioDir);
	Gpio_WriteHig_Pin(SOFT_UART_PORT, SOFT_UART_PIN);
}

void SOFTUART_Transmit(const char DataValue)
{
	/* Basic Logic

	TX pin is usually high. A high to low bit is the starting bit and
	a low to high bit is the ending bit. No parity bit. No flow control.
	BitCount is the number of bits to transmit. Data is transmitted LSB first.

	*/
	uint8_t i;
	// Send Start Bit
	UART_TX_PIN(0);
	delay_us(OneBitDelay);
	for(i = 0; i < 8; i++)
	{
		//Set Data pin according to the DataValue
		if(((DataValue >> i) & 0x1) == 0x1 ) //if Bit is high
		{
			UART_TX_PIN(1);
		}
		else //if Bit is low
		{
			UART_TX_PIN(0);
		}
		delay_us(OneBitDelay);
	}

	//Send Stop Bit
	UART_TX_PIN(1);
	delay_us(OneBitDelay);
}

void SOFTUART_TransmitString(const char* str)
{
	while(*str != 0)
	{
		SOFTUART_Transmit(*str);
		str++;
	}
}

void SOFTUART_TransmitStringValue(const char* str, ...)
{
	unsigned char buffer[20];
	va_list arg;
	va_start(arg, str);
	vsprintf((char*)buffer, str, arg);
	va_end(arg);
	SOFTUART_TransmitString((const char*)buffer);
}

void delay_us(unsigned long time)
{
	SysTick->LOAD = time * fac_us;
	SysTick->VAL = 0;
	SysTick->CTRL = 0x01;
	while(!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk));
	SysTick->CTRL = 0;
}



