/*
 * uartUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef UARTUSER_H
#define UARTUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define MAX_NUM_DATA 100
typedef struct
{
	uint8_t Data[MAX_NUM_DATA];
	uint8_t Byte;
	uint8_t Index;
	uint8_t State;
} buffer_t;


#define RX_GPIO_CLK  PD
#define UART_IPN     UART0
#define UART_PORT    HT_UART0
#define UART_IRQ     UART0_IRQn
#define UART_IRQ_Hdl UART0_IRQHandler
#define RX_GPIO_PORT HT_GPIOB
#define TX_GPIO_ID   GPIO_PB 
#define TX_GPIO_PIN  GPIO_PIN_7
#define RX_GPIO_ID   GPIO_PB
#define RX_GPIO_PIN  GPIO_PIN_8

#define DEBUG(str, ...) printf(str, ##__VA_ARGS__)
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*************************************************************************************************************
  * @brief  Configure the UxART
  * @retval None
  ***********************************************************************************************************/
void UxART_Configuration(void);

/*************************************************************************************************************
  * @brief  Transmit data
  * @retval None
  ***********************************************************************************************************/
void UxART_SendByte(uint8_t xByte);
void UxART_SendString(uint8_t *str);

/*************************************************************************************************************
  * @brief  Process Received data
  * @retval None
  ***********************************************************************************************************/
void UxART_Process(void);
#endif
#ifdef __cplusplus
}
#endif
