/*
 * gpioUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 * 
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef GPIOUSER_H
#define GPIOUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
typedef struct
{
  u32 gpioPort;
  u32 afioPin;
  AFIO_MODE_Enum afioMode;
  HT_GPIO_TypeDef* htGPIO;
  u16 gpioPin;
  GPIO_DIR_Enum gpioDir;
  GPIO_PR_Enum gpioPull;
  ControlStatus ctrl;
} GPIO_InitTypeDef;

#define _GPIO_USER_GPIOA_CLK_ENABLE(CKCU)  (CKCU.Bit.PA          = 1)
#define _GPIO_USER_GPIOB_CLK_ENABLE(CKCU)  (CKCU.Bit.PB          = 1)
#define _GPIO_USER_GPIOC_CLK_ENABLE(CKCU)  (CKCU.Bit.PC          = 1)
#define _GPIO_USER_GPIOD_CLK_ENABLE(CKCU)  (CKCU.Bit.PD          = 1)
#define _GPIO_USER_GLOBAL_CLK_ENABLE(CKCU) CKCU.Bit.AFIO         = 1; \
                                           CKCU_PeripClockConfig(CKCU, ENABLE)
#define _GPIO_USER_INTERRUPT_ENABLE(CKCU)  (CKCU.Bit.EXTI        = 1)


#define BTN1_PORTNUM  GPIO_PA
#define BTN1_PORT     HT_GPIOA
#define BTN1_PIN      GPIO_PIN_5
#define BTN1_AFIO_EXTI_CH AFIO_EXTI_CH_5
#define BTN1_EXTIO_SOURCE AFIO_ESS_PA
#define BTN1_EXTI_CHANNEL EXTI_CHANNEL_5
#define BTN1_EXTI_IRQn    EXTI5_IRQn
#define BTN1_IRQHandler   EXTI5_IRQHandler

#define LED1_PORTNUM  GPIO_PA
#define LED1_PORT     HT_GPIOA
#define LED1_PIN      GPIO_PIN_6
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */
extern uint8_t gBtnState;




/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*********************************************************************************************************//**
  * @brief  Configure the GPIO ports for input.
  * @retval None
  ***********************************************************************************************************/
void Gpio_In_Configuration(void);

/*********************************************************************************************************//**
  * @brief  Configure the GPIO ports for output.
  * @retval None
  ***********************************************************************************************************/
void Gpio_Out_Configuration(void);

/*********************************************************************************************************//**
  * @brief  Functions for Input mode.
  * @retval None
  ***********************************************************************************************************/
FlagStatus Gpio_Read_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n);
u16 Gpio_Read_Port(HT_GPIO_TypeDef* HT_GPIOx);

/*********************************************************************************************************//**
  * @brief  Functions for Output mode.
  * @retval None
  ***********************************************************************************************************/
void Gpio_Toggle_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n);
void Gpio_WriteVal_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n, FlagStatus stt);
void Gpio_WriteHig_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n);
void Gpio_WriteLow_Pin(HT_GPIO_TypeDef* HT_GPIOx, u16 GPIO_PIN_n);
void Gpio_WriteVal_Port(HT_GPIO_TypeDef* HT_GPIOx, u16 Data);

/*********************************************************************************************************//**
  * @brief  Functions for Interrupt mode.
  * @retval None
  ***********************************************************************************************************/
void Gpio_Interrupt_Configuration(void);
#endif
#ifdef __cplusplus
}
#endif
