/*
 * spiUser.c
 *
 *  Created on: Nov 02, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "spiUser.h"
#include "gpioUser.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */



/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */


/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */



/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void SPI_User_Configuration(void)
{
  /* !!! NOTICE !!!
     Notice that the local variable (structure) did not have an initial value.
     Please confirm that there are no missing members in the parameter settings below in this function.
  */
  SPI_InitTypeDef SPI_InitStructure;
	GPIO_InitTypeDef gpioInitStruct;

  CKCU_PeripClockConfig_TypeDef CKCUClock = {{ 0 }};
  CKCUClock.Bit.SPI_IDN       = 1;
  CKCUClock.Bit.AFIO       = 1;
  CKCU_PeripClockConfig(CKCUClock, ENABLE);


  AFIO_GPxConfig(SPI_SCK_GPIO_ID, SPI_SCK_AFIO_PIN, AFIO_FUN_SPI);
  AFIO_GPxConfig(SPI_MOSI_GPIO_ID, SPI_MOSI_AFIO_PIN, AFIO_FUN_SPI);
  AFIO_GPxConfig(SPI_MISO_GPIO_ID, SPI_MISO_AFIO_PIN, AFIO_FUN_SPI);
//   AFIO_GPxConfig(GPIO_PA, AFIO_PIN_3, AFIO_FUN_SPI);

  SPI_InitStructure.SPI_Mode = SPI_MASTER;
  // SPI_InitStructure.SPI_FIFO = SPI_FIFO_ENABLE;
  SPI_InitStructure.SPI_FIFO = SPI_FIFO_DISABLE;
  SPI_InitStructure.SPI_DataLength = SPI_DATALENGTH_8;
  // SPI_InitStructure.SPI_SELMode = SPI_SEL_HARDWARE;
  SPI_InitStructure.SPI_SELMode = SPI_SEL_SOFTWARE;
  SPI_InitStructure.SPI_SELPolarity = SPI_SELPOLARITY_LOW;
  SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_HIGH;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_SECOND;
  // SPI_InitStructure.SPI_RxFIFOTriggerLevel = 1;
  SPI_InitStructure.SPI_RxFIFOTriggerLevel = 0;
  SPI_InitStructure.SPI_TxFIFOTriggerLevel = 0;
  SPI_InitStructure.SPI_ClockPrescaler = 8;
  SPI_Init(SPI_PORT, &SPI_InitStructure);

  SPI_SELOutputCmd(SPI_PORT, ENABLE);

  // SPI_IntConfig(SPI_PORT, SPI_INT_RXBNE, ENABLE);
  // NVIC_EnableIRQ(SPI_IRQ);

  SPI_Cmd(SPI_PORT, ENABLE);

  _GPIO_USER_GPIOA_CLK_ENABLE(CKCUClock);
  _GPIO_USER_GLOBAL_CLK_ENABLE(CKCUClock);
	
  gpioInitStruct.gpioPort = SPI_SEL1_PORTNUM;
  gpioInitStruct.afioPin = (u32)SPI_SEL1_PIN;
  gpioInitStruct.afioMode = AFIO_FUN_GPIO;
  gpioInitStruct.htGPIO = SPI_SEL1_PORT;
  gpioInitStruct.gpioPin = SPI_SEL1_PIN;
  gpioInitStruct.gpioDir = GPIO_DIR_OUT;
  // gpioInitStruct.gpioPull = GPIO_PR_DISABLE;
  // gpioInitStruct.ctrl = ENABLE;

  AFIO_GPxConfig(gpioInitStruct.gpioPort, gpioInitStruct.afioPin, gpioInitStruct.afioMode);
  GPIO_DirectionConfig(gpioInitStruct.htGPIO, gpioInitStruct.gpioPin, gpioInitStruct.gpioDir);
	Gpio_WriteHig_Pin(SPI_SEL1_PORT, SPI_SEL1_PIN);
}

u8 SPI_User_SendReceive(u8 data)
{
  /* Loop while Tx buffer register is empty                                                                 */
  while (!SPI_GetFlagStatus(SPI_PORT, SPI_FLAG_TXBE));
  SPI_SendData(SPI_PORT, data);
  /* Loop while Rx is not empty                                                                             */
  while (!SPI_GetFlagStatus(SPI_PORT, SPI_FLAG_RXBNE));
	return (SPI_ReceiveData(SPI_PORT));
}

void SPI_User_SendMulti(u8 *p, u32 len)
{
  while(len--) SPI_User_SendReceive(*p++);
}

void SPI_User_ReceiveMulti(u8 *p, u32 len)
{
  u32 i = 0;
  for(i = 0; i < len; i++) p[i] = SPI_User_SendReceive(0);
}
